## CRI-O compilation

[![pipeline status](https://gitlab.com/ymasson/cri-o_build/badges/master/pipeline.svg)](https://gitlab.com/ymasson/cri-o_build/commits/master)


This is a simple way to compile CRI-O binaries with Kpod included.

The version is set in .gitlab-ci.yml.


CRI-o project: https://github.com/kubernetes-incubator/cri-o
Kpod project: https://github.com/projectatomic/libpod

Sources: https://github.com/kubernetes-incubator/cri-o/archive/v${CRIO_VERSION}.tar.gz


### Artefacts:
```
conmon
cri-o-v${CRIO_VERSION}.tar.gz
crio
crio.conf
crioctl
pause
podman
policy.json
seccomp.json
```


### Download :
All artefacts can be downloaded on https://ymasson.gitlab.io/cri-o_build/{ARTEFACT}


